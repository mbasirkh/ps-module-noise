#include <TDirectoryFile.h>
#include <TFile.h>
#include <TList.h>
#include <TCanvas.h>
#include <TF1.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <TMath.h>
#include <TTree.h>

TFile* _file0;
TObject* myObject;
TH1F* hsigma = nullptr;
TH1F* hmu = nullptr;
TCanvas* myCanvas;
TTree* fitTreeSSA;
double fitParamsSSA[10];
TTree* fitTreeMPA;
double fitParamsMPA[10];
std::vector<TH1F*> getCurves(int hybridId, std::string chipType, int chipId) {
   int iCurve;
   std::vector<TH1F*> result;
   std::string dirName = Form("Detector/Board_0/OpticalGroup_2/Hybrid_%d/%s_%d/Channel", hybridId, chipType.c_str(), chipId);
   TDirectoryFile* myDir = (TDirectoryFile*)_file0->Get(dirName.c_str());
   TList* myList = myDir->GetListOfKeys();
   for (iCurve = 0 ; iCurve < myList->GetSize(); ++iCurve) {
     TKey* myKey = (TKey*) myList->At(iCurve);
     myObject = myKey->ReadObj();
     if  (myObject && (std::string(myObject->ClassName()) == "TH1F")) {
       TH1F* myCurve = (TH1F*)myKey->ReadObj();
       result.push_back(myCurve);
     } else {
       std::cerr << "ERROR: I found a missing object, or non-TH1F" << std::endl;
     }
   }
   return result;
}

double noiseFunction(Double_t *x, Double_t *par) {
    double xx = x[0];
    double a = par[0];
    double sigma_a = par[1];
    double b = par[2];
    double sigma_b = par[3];
    double c = par[4];
    double sigma_c = par[5];
    double N = par[6];

    const double SQ2 = TMath::Sqrt(2);
    double xa = (xx-a)/SQ2/sigma_a;
    double xb = (xx-b)/SQ2/sigma_b;
    double xc = (xx-c)/SQ2/sigma_c;

    double term1 = 1 + TMath::Erf(xa);
    double term2 = 1 - TMath::Erf(xb);
    double term3 = 1 - TMath::Power(TMath::Erf(xc), 2);
    
    return 0.25 * term1 * term2 + N * term3;
}
 void getAndFitSSA(int hybridId, std::string chipType, int chipId, TH1D* myNoiseHist = nullptr){
    auto myCurves = getCurves(hybridId, chipType, chipId);
    double min_range = 0;
    double max_range = 255;
    TF1 *myFunctionSSA = new TF1("myFunctionSSA", noiseFunction, min_range, max_range, 7);
    
    myFunctionSSA->SetParName(0, "a");
    myFunctionSSA->SetParName(1, "sigma_a");
    myFunctionSSA->SetParName(2, "b");
    myFunctionSSA->SetParName(3, "sigma_b");
    myFunctionSSA->SetParName(4, "c");
    myFunctionSSA->SetParName(5, "sigma_c");
    myFunctionSSA->SetParName(6, "N");
    
     // Set parameter limits for myFunction
    myFunctionSSA->SetParLimits(0, 0, 300);
    myFunctionSSA->SetParLimits(1, 0, 50);
    myFunctionSSA->SetParLimits(2, 0, 300);
    myFunctionSSA->SetParLimits(3, 0, 50);
    myFunctionSSA->SetParLimits(4, 0, 300);
    myFunctionSSA->SetParLimits(5, 0, 50);
    myFunctionSSA->SetParLimits(6, 0, 100);
      
    fitTreeSSA = new TTree("fitTreeSSA", "Fit Parameters Tree for SSA");
    // Creating branches for each fit parameter SSA
    
    fitTreeSSA->Branch("a", &fitParamsSSA[0], "a/D");
    fitTreeSSA->Branch("sigma_a", &fitParamsSSA[1], "sigma_a/D");
    fitTreeSSA->Branch("b", &fitParamsSSA[2], "b/D");
    fitTreeSSA->Branch("sigma_b", &fitParamsSSA[3], "sigma_b/D");
    fitTreeSSA->Branch("c", &fitParamsSSA[4], "c/D");
    fitTreeSSA->Branch("sigma_c", &fitParamsSSA[5], "sigma_c/D");
    fitTreeSSA->Branch("N", &fitParamsSSA[6], "N/D");
    fitTreeSSA->Branch("Chisquare", &fitParamsSSA[7], "Chisquare/D");
    fitTreeSSA->Branch("NDF", &fitParamsSSA[8], "NDF/D");
    fitTreeSSA->Branch("channelId", &fitParamsSSA[9], "channelId/D");
    //int currentChannel = 0;
    int channelId=1;
    for (auto aCurve : myCurves) {
           double firstX_aboveHalf = 0, lastX_belowHalf = 0, maxX = 0, firstX_at60PercentOfMax = 0;
           double maxContent = 0;
    
    // Derivatives
    double derivativeFirst = 0, derivativeLast = 0;

    bool foundFirst = false;
    bool foundLast=false;
           double pp, qq,h;
            double N_inj = 500;
            int Nbins = aCurve->GetNbinsX();
            for (int iBin = 1; iBin <= Nbins; ++iBin) {
                //if (aCurve->GetBinContent(iBin) > 1) {
                    //aCurve->SetBinError(iBin, 0.1);// initially we put it as 0.1 but it was not working for SSA, 0.05 works for SSA but no MPA(0.1) 
                //}
             double y=aCurve->GetBinContent(iBin);
if (y<=1) {
  // y<=1 binomial error
  h=y*N_inj;
  if (h==0) {
     pp=0.5/N_inj;
  } else if (h==N_inj) {
     pp=(N_inj-0.5)/N_inj;
  } else {
     pp=y;
  }
  qq=1-pp;
  aCurve->SetBinError(iBin,sqrt(pp*qq/N_inj));
} else {
  // y>1 : poisson error on the extra counts
  aCurve->SetBinError(iBin,sqrt((y-1)/N_inj));
}
            }
            for (int i = 1; i <= aCurve->GetNbinsX(); ++i) {  
        double binCenter = aCurve->GetBinCenter(i);
        double content = aCurve->GetBinContent(i);
        double prevContent = aCurve->GetBinContent(i-1);

        // Check if we've encountered the maximum value
        if (content > maxContent) {
            maxContent = content;
            maxX = binCenter;
        }

        // Find the first and last x where the curve crosses 0.5
        if (content > 0.5 && !foundFirst) {
            foundFirst = true;
            firstX_aboveHalf = binCenter;
            // Calculate the derivative at this point
            derivativeFirst = (content-prevContent) / (aCurve->GetBinWidth(i));
        }

        if (prevContent>0.5 && content < 0.5) {
            foundLast = true;
            lastX_belowHalf = aCurve->GetBinCenter(i-1);  // previous bin was the last above 0.5
            // Calculate the derivative at this point
            derivativeLast = (content-prevContent) / (aCurve->GetBinWidth(i));
        }
    }
        double a = firstX_aboveHalf;
    double b = lastX_belowHalf;
    double c =  maxX ;
    double sigma_a =fabs(derivativeFirst);
    double sigma_b =fabs(derivativeLast) ;
    double sigma_c =fabs(derivativeFirst) ; //it should be around 0.140//firstX_at60PercentOfMax;
    double N = aCurve->GetMaximum()-1;
    sigma_a = 2.5;
    sigma_b = 2.5;
    sigma_c = 2.5;
            // Set the parameters for the function
              myFunctionSSA->SetParameters(a, sigma_a, b, sigma_b, c, sigma_c, N);
            
            /*for(int i=0;i<7;i++)
            {
            fitParamsSSA[i]=0.0;            
            //std::cout<<fitParamsSSA[i]<<endl;
            }*/
           // std::cout << "Fitting channel " << currentChannel << "...\n";
            aCurve->Fit(myFunctionSSA, "Q");
            // Save fit parameters to the arrays and fill the trees
            fitParamsSSA[0] = myFunctionSSA->GetParameter(0);
            fitParamsSSA[1] = myFunctionSSA->GetParameter(1);
            fitParamsSSA[2] = myFunctionSSA->GetParameter(2);
            fitParamsSSA[3] = myFunctionSSA->GetParameter(3);
            fitParamsSSA[4] = myFunctionSSA->GetParameter(4);
            fitParamsSSA[5] = myFunctionSSA->GetParameter(5);
            fitParamsSSA[6] = myFunctionSSA->GetParameter(6);
            fitParamsSSA[7] = myFunctionSSA->GetChisquare();
            fitParamsSSA[8] = myFunctionSSA->GetNDF();
            fitParamsSSA[9] = static_cast<double>(channelId);
            if(fitParamsSSA[4]<13)
            { 
            fitTreeSSA->Fill();
            }
           // Draw the fit on top of the histogram
            aCurve->Draw();
            myFunctionSSA->Draw("same");
            gStyle->SetOptFit(11111);
            channelId++;
        }//main for loop 
        
                    // ... (store other fit parameters for SSA)
}//main function

void getAndFitMPA(int hybridId, std::string chipType, int chipId, TH1D* myNoiseHist = nullptr) {
    auto myCurves = getCurves(hybridId, chipType, chipId);
    double min_range = 0;
    double max_range = 255;
    
    fitTreeMPA = new TTree("fitTreeMPA", "Fit Parameters Tree for MPA");
    
    fitTreeMPA->Branch("a", &fitParamsMPA[0], "a/D");
    fitTreeMPA->Branch("sigma_a", &fitParamsMPA[1], "sigma_a/D");
    fitTreeMPA->Branch("b", &fitParamsMPA[2], "b/D");
    fitTreeMPA->Branch("sigma_b", &fitParamsMPA[3], "sigma_b/D");
    fitTreeMPA->Branch("c", &fitParamsMPA[4], "c/D");
    fitTreeMPA->Branch("sigma_c", &fitParamsMPA[5], "sigma_c/D");
    fitTreeMPA->Branch("N", &fitParamsMPA[6], "N/D");
    fitTreeMPA->Branch("Chisquare", &fitParamsMPA[7], "Chisquare/D");
    fitTreeMPA->Branch("NDF", &fitParamsMPA[8], "NDF/D");
    fitTreeMPA->Branch("channelId", &fitParamsMPA[9], "channelId/D");
    //fitTreeMPA->Branch("chi2_NDF", &fitParamsMPA[9], "chi2_NDF");
    
    TF1 *myFunctionMPA = new TF1("myFunctionMPA", noiseFunction, min_range, max_range, 7);
    myFunctionMPA->SetParName(0, "a");
    myFunctionMPA->SetParName(1, "sigma_a");
    myFunctionMPA->SetParName(2, "b");
    myFunctionMPA->SetParName(3, "sigma_b");
    myFunctionMPA->SetParName(4, "c");
    myFunctionMPA->SetParName(5, "sigma_c");
    myFunctionMPA->SetParName(6, "N");
    
     // Set parameter limits for myFunction
    myFunctionMPA->SetParLimits(0, 0, 300);
    myFunctionMPA->SetParLimits(1, 0, 50);
    myFunctionMPA->SetParLimits(2, 0, 300);
    myFunctionMPA->SetParLimits(3, 0, 50);
    myFunctionMPA->SetParLimits(4, 0, 300);
    myFunctionMPA->SetParLimits(5, 0, 50);
    myFunctionMPA->SetParLimits(6, 0, 100);
    

    //int currentChannel = 0;
    int channelId=1;
    for (auto aCurve : myCurves) {
           double firstX_aboveHalf = 0, lastX_belowHalf = 0, maxX = 0, firstX_at60PercentOfMax = 0;
           double maxContent = 0;
    
    // Derivatives
    double derivativeFirst = 0, derivativeLast = 0;

    bool foundFirst = false;
    bool foundLast=false;
            double pp, qq,h;
            double N_inj = 500;
            int Nbins = aCurve->GetNbinsX();
            for (int iBin = 1; iBin <= Nbins; ++iBin) {
                //if (aCurve->GetBinContent(iBin) > 1) {
                    //aCurve->SetBinError(iBin, 0.1);// initially we put it as 0.1 but it was not working for SSA, 0.05 works for SSA but no MPA(0.1) 
                //}
             double y=aCurve->GetBinContent(iBin);
if (y<=1) {
  // y<=1 binomial error
  h=y*N_inj;
  if (h==0) {
     pp=0.5/N_inj;
  } else if (h==N_inj) {
     pp=(N_inj-0.5)/N_inj;
  } else {
     pp=y;
  }
  qq=1-pp;
  aCurve->SetBinError(iBin,sqrt(pp*qq/N_inj));
} else {
  // y>1 : poisson error on the extra counts
  aCurve->SetBinError(iBin,sqrt((y-1)/N_inj));
}
            }
            for (int i = 1; i <= aCurve->GetNbinsX(); ++i) {  
        double binCenter = aCurve->GetBinCenter(i);
        double content = aCurve->GetBinContent(i);
        double prevContent = aCurve->GetBinContent(i-1);

        // Check if we've encountered the maximum value
        if (content > maxContent) {
            maxContent = content;
            maxX = binCenter;
        }

        // Find the first and last x where the curve crosses 0.5
        if (content > 0.5 && !foundFirst) {
            foundFirst = true;
            firstX_aboveHalf = binCenter;
            // Calculate the derivative at this point
            derivativeFirst = (content-prevContent) / (aCurve->GetBinWidth(i));
        }

        if (prevContent>0.5 && content < 0.5) {
            foundLast = true;
            lastX_belowHalf = aCurve->GetBinCenter(i-1);  // previous bin was the last above 0.5
            // Calculate the derivative at this point
            derivativeLast = (content-prevContent) / (aCurve->GetBinWidth(i));
        }
    }
        double a = firstX_aboveHalf;
    double b = lastX_belowHalf;
    double c =  maxX ;
    double sigma_a =fabs(derivativeFirst);
    double sigma_b =fabs(derivativeLast) ;
    double sigma_c =fabs(derivativeFirst) ; //it should be around 0.140//firstX_at60PercentOfMax;
    double N = aCurve->GetMaximum()-1;
    sigma_a = 2.5;
    sigma_b = 2.5;
    sigma_c = 2.5;
            // Set the parameters for the function
              myFunctionMPA->SetParameters(a, sigma_a, b, sigma_b, c, sigma_c, N); // Replace with actual values

            aCurve->Fit(myFunctionMPA, "Q");
        //  Save fit parameters to the arrays and fill the trees
            fitParamsMPA[0] = myFunctionMPA->GetParameter(0);
            fitParamsMPA[1] = myFunctionMPA->GetParameter(1);
            fitParamsMPA[2] = myFunctionMPA->GetParameter(2);
            fitParamsMPA[3] = myFunctionMPA->GetParameter(3);
            fitParamsMPA[4] = myFunctionMPA->GetParameter(4);
            fitParamsMPA[5] = myFunctionMPA->GetParameter(5);
            fitParamsMPA[6] = myFunctionMPA->GetParameter(6);
            fitParamsMPA[7] = myFunctionMPA->GetChisquare();
            fitParamsMPA[8] = myFunctionMPA->GetNDF();
            fitParamsMPA[9] =  static_cast<double>(channelId);
            if(fitParamsMPA[4]<60)
            {
           fitTreeMPA->Fill();
            }
            // Draw the fit on top of the histogram
           aCurve->Draw();
           myFunctionMPA->Draw("same");
           gStyle->SetOptFit(11111);
             channelId++;
        }//main for loop 
        
    }//main function
void noise_chip_tree() {
 _file0 = TFile::Open("Hybrid_20.root");
  getAndFitSSA(4, "SSA", 0);
  getAndFitMPA(4, "MPA", 8);
  /*TCanvas* chi2Canvas = new TCanvas("chi2Canvas", "Chi2/NDF Distribution", 800, 600);
    //chi2Canvas->SetLogy(); // Optional: Use log scale for the y-axis

    TH1F* chi2HistSSA = new TH1F("chi2HistSSA", "#chi^{2}/NDF Distribution (SSA)", 100, 0, 10); // Adjust binning and range as needed
    fitTreeSSA->Draw("chi2_NDF>>chi2HistSSA", "chi2_NDF < 2"); // cut on chi2_NDF values less than 2

    TH1F* chi2HistMPA = new TH1F("chi2HistMPA", "#chi^{2}/NDF Distribution (MPA)", 100, 0, 10); // Adjust binning and range as needed
    fitTreeMPA->Draw("chi2_NDF>>chi2HistMPA", "chi2_NDF < 2"); //MPA

    // Calculate and display the average chi-squared
    double avgChi2SSA = chi2HistSSA->GetMean();
    double avgChi2MPA = chi2HistMPA->GetMean();

    // Add labels and legend
    chi2HistSSA->GetXaxis()->SetTitle("#chi^{2}/NDF");
    chi2HistSSA->GetYaxis()->SetTitle("Number of Fits");
    chi2HistSSA->SetTitle("Chi2/NDF Distribution for SSA");
    chi2HistSSA->Draw();
    chi2HistMPA->SetLineColor(kRed);
    chi2HistMPA->Draw("same");

    TLegend* legend = new TLegend(0.7, 0.8, 0.9, 0.9);
    legend->AddEntry(chi2HistSSA, "SSA", "l");
    legend->AddEntry(chi2HistMPA, "MPA", "l");
    legend->Draw();

    std::cout << "Average Chi2/NDF for SSA: " << avgChi2SSA << std::endl;
    std::cout << "Average Chi2/NDF for MPA: " << avgChi2MPA << std::endl;

    // Save or display the canvas as needed
    chi2Canvas->SaveAs("Chi2NDFDistribution.png");*/

    TFile* outputRootFile = TFile::Open("20_cold.root", "RECREATE");
    fitTreeSSA->Write();
    fitTreeMPA->Write();
    outputRootFile->Close();
}

